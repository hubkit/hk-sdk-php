<?php

namespace Hubkit\Sdk;

use Hubkit\Sdk\Client;
use Hubkit\Sdk\Query\Activity\ActivityQuery;
use Hubkit\Sdk\Query\Activity\AllActivitiesQuery;
use Hubkit\Sdk\Query\Device\ActivateDeviceQuery;
use Hubkit\Sdk\Query\Device\CreateDeviceQuery;
use Hubkit\Sdk\Query\Device\DeviceQuery;
use Hubkit\Sdk\Query\Device\UpdateDeviceQuery;
use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\RawData\CreateRawDataQuery;
use Hubkit\Sdk\Query\RawData\RawDataQuery;
use Hubkit\Sdk\Query\Session\AlgorithmProcessQuery;
use Hubkit\Sdk\Query\Session\CreateSessionQuery;
use Hubkit\Sdk\Query\Session\DeleteSessionQuery;
use Hubkit\Sdk\Query\Session\ReadySessionQuery;
use Hubkit\Sdk\Query\Session\SessionQuery;
use Hubkit\Sdk\Query\User\MeQuery;
use Hubkit\Sdk\Response;
use Hubkit\Sdk\Service;

/**
 * HubkitSdk
 */
class HubkitSdk implements HubkitClientInterface
{
    /**
     * service
     *
     * @var Service
     */
    private $service;

    /**
     * service
     *
     * @var Client
     */
    private $client;

    /**
     * Constructor
     *
     * @param Client $client
     */
    public function __construct(Client $client = null)
    {
        if (!$client) {
            $client = new Client();
        }

        $this->client = $client;
        $this->service = new Service($this->client);
    }

    /**
     * me
     *
     * @return Response
     */
    public function me(): Response
    {
        $query = new meQuery();

        return $this->service->query($query);
    }

    /**
     * device
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function device(string $uuid): Response
    {
        $query = new DeviceQuery();
        $query->setUuid($uuid);

        return $this->service->query($query);
    }

    /**
     * createDevice
     *
     * @param string $datas
     *
     * @return Response
     */
    public function createDevice(array $datas): Response
    {
        $query = new CreateDeviceQuery();
        $query->setDatas($datas);

        return $this->service->query($query);
    }

    /**
     * updateDevice
     *
     * @param string $uuid
     * @param array  $datas
     *
     * @return Response
     */
    public function updateDevice(
        string $uuid,
        array  $datas
    ): Response {
        $query = new UpdateDeviceQuery();
        $query->setUuid($uuid);
        $query->setDatas($datas);

        return $this->service->query($query);
    }

    /**
     * activateDevice
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function activateDevice(string $uuid): Response
    {
        $query = new ActivateDeviceQuery();
        $query->setUuid($uuid);

        return $this->service->query($query);
    }

    /**
     * activity
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function activity(string $uuid): Response
    {
        $query = new ActivityQuery();
        $query->setUuid($uuid);

        return $this->service->query($query);
    }

    /**
     * allActivities
     *
     * @return Response
     */
    public function allActivities(): Response
    {
        $query = new AllActivitiesQuery();

        return $this->service->query($query);
    }

    /**
     * session
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function session(string $uuid): Response
    {
        $query = new SessionQuery();
        $query->setUuid($uuid);

        return $this->service->query($query);
    }

    /**
     * deleteSession
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function deleteSession(string $uuid): Response
    {
        $query = new DeleteSessionQuery();
        $query->setUuid($uuid);

        return $this->service->query($query);
    }

    /**
     * readySession
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function readySession(string $uuid): Response
    {
        $query = new ReadySessionQuery();
        $query->setUuid($uuid);

        return $this->service->query($query);
    }

    /**
     * createSession
     *
     * @param array $datas
     *
     * @return Response
     */
    public function createSession(array $datas): Response
    {
        $query = new CreateSessionQuery();
        $query->setDatas($datas);

        return $this->service->query($query);
    }

    /**
     * rawData
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function rawData(string $uuid): Response
    {
        $query = new RawDataQuery();
        $query->setUuid($uuid);

        return $this->service->query($query);
    }

    /**
     * createRawData
     *
     * @param array $datas
     * @param mixed $file
     *
     * @return Response
     */
    public function createRawData(array $datas, $file): Response
    {
        $query = new CreateRawDataQuery();
        $query->setForm($datas);
        $query->setFile($file);

        return $this->service->query($query);
    }

    public function algorithmProcess(string $uuid, array $datas): Response
    {
        $query = new AlgorithmProcessQuery();
        $query->setUuid($uuid);
        $query->setDatas($datas);

        return $this->service->query($query);
    }

    /**
     * setApiKey
     *
     * @param string $apiKey
     *
     * @return self
     */
    public function setApiKey(string $apiKey): HubkitClientInterface
    {
        $this->service->setApiKey($apiKey);

        return $this;
    }

    /**
     * setBaseUrl
     *
     * @param string $baseUrl
     *
     * @return self
     */
    public function setBaseUrl(string $baseUrl): HubkitClientInterface
    {
        $this->service->setBaseUrl($baseUrl);

        return $this;
    }
}
