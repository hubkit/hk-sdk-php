<?php

namespace Hubkit\Sdk;

use Hubkit\Sdk\Client;
use Hubkit\Sdk\Query\QueryInterface;

/**
 * Service
 */
class Service
{
    public $baseUrl = 'http://localhost:8080/api/v1';

    /**
     * client
     *
     * @var Client
     */
    public $client;

    /**
     * Constructor
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * setBaseUrl
     *
     * @param string $baseUrl
     *
     * @return self
     */
    public function setBaseUrl(string $baseUrl): self
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    /**
     * setApiKey
     *
     * @param string $apiKey
     *
     * @return self
     */
    public function setApiKey(string $apiKey): void
    {
        $this->client->addHeader('apiKey', sprintf('%s', $apiKey));
    }

    /**
     * query
     *
     * @param QueryInterface $query
     *
     * @return Response
     */
    public function query(QueryInterface $query)
    {
        $url = $this->createUrl($query->getUrl());
        $this->client->setMethod($query->getMethod());
        $this->client->setBody($query->getDatas());
        $this->client->setForm($query->getForm());
        $this->client->setFiles($query->getFiles());

        return $this->client->send($url);
    }

    /**
     * createUrl
     *
     * @param string $url
     *
     * @return string
     */
    public function createUrl(string $url): string
    {
        return sprintf('%s/%s', $this->baseUrl, $url);
    }
}
