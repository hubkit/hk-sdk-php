<?php

namespace Hubkit\Sdk;

use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\StreamFactoryDiscovery;
use Http\Client\Common\HttpMethodsClient;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use Psr\Http\Message\UploadedFileInterface;

/**
 * Client
 */
class Client
{
    /**
     * @var string
     */
    private $method = 'GET';

    /**
     * @var string
     */
    private $headers = [];

    /**
     * @var mixed
     */
    private $body = [];

    /**
     * @var array
     */
    private $form = [];

    /**
     * @var array
     */
    private $files = [];

    /**
     * @var StreamFactory
     */
    private $streamFactory;

    /**
     * @var MultipartStreamBuilder
     */
    private $builder;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->streamFactory = StreamFactoryDiscovery::find();
        $this->builder = new MultipartStreamBuilder($this->streamFactory);
    }

    /**
     * setMethod
     *
     * @param string $method
     *
     * @return self
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Sets the value of form
     *
     * @param array $form
     *
     * @return Client
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Sets the value of files
     *
     * @param array $files
     *
     * @return Client
     */
    public function setFiles(array $files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Sets the value of body
     *
     * @param string $datas
     *
     * @return Client
     */
    public function setBody($datas)
    {
        $this->body = $datas;

        return $this;
    }

    /**
     * addHeader
     *
     * @param string $key
     * @param mixed  $value
     */
    public function addHeader(string $key, $value)
    {
        $this->headers[$key] = $value;
    }

    /**
     * send
     *
     * @param string $url
     *
     * @return Response
     */
    public function send(string $url): Response
    {
        $client = HttpClientDiscovery::find();
        $messageFactory = MessageFactoryDiscovery::find();

        if ($this->form || $this->files) {
            $this->createForm();
        }

        $response = $client->sendRequest(
            $messageFactory->createRequest($this->method, $url, $this->headers, $this->body)
        );

        return new Response($response);
    }

    /**
     * createForm
     */
    private function createForm()
    {
        foreach ($this->form as $fieldName => $multipartField) {
            $this->builder->addResource($fieldName, $multipartField);
        }

        foreach ($this->files as $fieldName => $file) {
            if (!$file instanceof UploadedFileInterface) {
                throw new \Exception('This file must implements UploadedFileInterface class');
            }

            $this->builder->addResource($fieldName, $file->getStream(), ['filename' => $file->getClientFilename()]);
        }

        $this->headers['Content-Type'] = 'multipart/form-data; boundary="'.$this->builder->getBoundary().'"';
        $this->body = $this->builder->build();
    }
}
