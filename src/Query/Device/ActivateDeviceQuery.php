<?php

namespace Hubkit\Sdk\Query\Device;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * ActivateDeviceQuery
 */
class ActivateDeviceQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'devices/%s/activate';

    const METHOD = 'PATCH';

    public $uuid;

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return CreateDeviceQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf($this::URL, $this->uuid);
    }
}
