<?php

namespace Hubkit\Sdk\Query\Device;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * CreateDeviceQuery
 */
class CreateDeviceQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'devices';

    const METHOD = 'POST';

    public $datas;

    /**
     * getDatas
     *
     * @return string
     */
    public function getDatas()
    {
        return json_encode($this->datas);
    }

    /**
     * Sets the value of datas
     *
     * @param array $datas
     *
     * @return UpdateDeviceQuery
     */
    public function setDatas(array $datas)
    {
        $this->datas = $datas;

        return $this;
    }
}
