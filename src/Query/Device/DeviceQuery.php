<?php

namespace Hubkit\Sdk\Query\Device;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * DeviceQuery
 */
class DeviceQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'devices';

    const METHOD = 'GET';

    public $uuid;

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return DeviceQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf("%s/%s", $this::URL, $this->uuid);
    }
}
