<?php

namespace Hubkit\Sdk\Query\Device;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * UpdateDeviceQuery
 */
class UpdateDeviceQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'devices';

    const METHOD = 'PATCH';

    public $uuid;
    public $datas;

    /**
     * getDatas
     *
     * @return string
     */
    public function getDatas()
    {
        return json_encode($this->datas);
    }

    /**
     * Sets the value of datas
     *
     * @param array $datas
     *
     * @return UpdateDeviceQuery
     */
    public function setDatas(array $datas)
    {
        $this->datas = $datas;

        return $this;
    }

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return CreateDeviceQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf("%s/%s", $this::URL, $this->uuid);
    }
}
