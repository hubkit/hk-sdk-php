<?php

namespace Hubkit\Sdk\Query\Session;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * CreateSessionQuery
 */
class CreateSessionQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'sessions';

    const METHOD = 'POST';

    public $datas;

    /**
     * getDatas
     *
     * @return string
     */
    public function getDatas()
    {
        return json_encode($this->datas);
    }

    /**
     * Sets the value of datas
     *
     * @param array $datas
     *
     * @return CreateSessionQuery
     */
    public function setDatas(array $datas)
    {
        $this->datas = $datas;

        return $this;
    }
}
