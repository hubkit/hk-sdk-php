<?php

namespace Hubkit\Sdk\Query\Session;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * ReadySessionQuery
 */
class ReadySessionQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'sessions/%s/ready';

    const METHOD = 'PATCH';

    public $uuid;

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return ReadySessionQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf($this::URL, $this->uuid);
    }
}
