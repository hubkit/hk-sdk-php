<?php

namespace Hubkit\Sdk\Query\Session;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * AlgorithmProcessQuery
 */
class AlgorithmProcessQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'algorithm/process/%s';

    const METHOD = 'POST';

    public $uuid;

    public $datas;

    /**
     * getDatas
     *
     * @return string
     */
    public function getDatas()
    {
        return json_encode($this->datas);
    }

    /**
     * Sets the value of datas
     *
     * @param array $datas
     *
     * @return CreateSessionQuery
     */
    public function setDatas(array $datas)
    {
        $this->datas = $datas;

        return $this;
    }

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return ReadySessionQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf($this::URL, $this->uuid);
    }
}
