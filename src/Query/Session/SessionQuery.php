<?php

namespace Hubkit\Sdk\Query\Session;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * SessionQuery
 */
class SessionQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'sessions';

    const METHOD = 'GET';

    public $uuid;

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return SessionQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf("%s/%s", $this::URL, $this->uuid);
    }
}
