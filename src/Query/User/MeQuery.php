<?php

namespace Hubkit\Sdk\Query\User;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * MeQuery
 */
class MeQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'me';

    const METHOD = 'GET';
}
