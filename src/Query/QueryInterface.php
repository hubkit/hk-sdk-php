<?php

namespace Hubkit\Sdk\Query;

/**
 * QueryInterface
 */
interface QueryInterface
{
    /**
     * @access public
     *
     * @return string
     */
    public function getUrl(): string;

    /**
     * @access public
     *
     * @return string
     */
    public function getMethod(): string;

    /**
     * @access public
     */
    public function getDatas();

    /**
     * @access public
     */
    public function getForm(): array;

    /**
     * @access public
     */
    public function getFiles(): array;
}
