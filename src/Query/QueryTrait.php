<?php

namespace Hubkit\Sdk\Query;

/**
 * QueryTrait
 */
trait QueryTrait
{
    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this::URL;
    }

    /**
     * Gets the value of method.
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this::METHOD;
    }

    /**
     * Gets the value of Datas.
     *
     * @return string
     */
    public function getDatas(): string
    {
        return json_encode([]);
    }

    /**
     * Gets the value of Multipart.
     *
     * @return array
     */
    public function getForm(): array
    {
        return [];
    }

    /**
     * Gets the value of Multipart files.
     *
     * @return array
     */
    public function getFiles(): array
    {
        return [];
    }
}
