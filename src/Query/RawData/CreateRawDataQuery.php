<?php

namespace Hubkit\Sdk\Query\RawData;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * CreateRawDataQuery
 */
class CreateRawDataQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'raw_datas';

    const METHOD = 'POST';

    public $form;

    public $files;

    /**
     * getForm
     *
     * @return array
     */
    public function getForm(): array
    {
        return $this->form;
    }

    /**
     * Sets the value of form
     *
     * @param array $form
     *
     * @return UpdateRawDataQuery
     */
    public function setForm(array $form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * setFile
     *
     * @param mixed $file
     *
     * @return this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Gets the value of files.
     *
     * @return array
     */
    public function getFiles(): array
    {
        return ['file' => $this->file];
    }
}
