<?php

namespace Hubkit\Sdk\Query\RawData;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * RawDataQuery
 */
class RawDataQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'raw_datas';

    const METHOD = 'GET';

    public $uuid;

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return RawDataQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf("%s/%s", $this::URL, $this->uuid);
    }
}
