<?php

namespace Hubkit\Sdk\Query\Activity;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * AllActivitiesQuery
 */
class AllActivitiesQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'activities';

    const METHOD = 'GET';
}
