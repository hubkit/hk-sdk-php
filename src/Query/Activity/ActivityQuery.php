<?php

namespace Hubkit\Sdk\Query\Activity;

use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Query\QueryTrait;

/**
 * ActivityQuery
 */
class ActivityQuery implements QueryInterface
{
    use QueryTrait;

    const URL = 'activities';

    const METHOD = 'GET';

    public $uuid;

    /**
     * Sets the value of uuid
     *
     * @param string $uuid
     *
     * @return ActivityQuery
     */
    public function setUuid(string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Gets the value of url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf("%s/%s", $this::URL, $this->uuid);
    }
}
