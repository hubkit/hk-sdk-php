<?php

namespace Hubkit\Sdk;

use Hubkit\Sdk\Response;

/**
 * HubkitClientInterface
 *
 */
interface HubkitClientInterface
{
    public function me(): Response;

    public function device(string $uuid): Response;

    public function createDevice(array $datas): Response;

    public function updateDevice(string $uuid, array $datas): Response;

    public function activateDevice(string $uuid): Response;

    public function activity(string $uuid): Response;

    public function allActivities(): Response;

    public function session(string $uuid): Response;

    public function deleteSession(string $uuid): Response;

    public function readySession(string $uuid): Response;

    public function createSession(array $datas): Response;

    public function rawData(string $uuid): Response;

    public function createRawData(array $datas, $file): Response;

    public function algorithmProcess(string $uuid, array $datas): Response;

    public function setApiKey(string $apiKey): HubkitClientInterface;

    public function setBaseUrl(string $baseUrl): HubkitClientInterface;
}
