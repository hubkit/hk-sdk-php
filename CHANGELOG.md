#CHANGELOG

## 1.0.8

    * Update README

## 1.0.7

    * Fix unit test

## 1.0.6

    * Add delete session method

## 1.0.5

    * Fix issue with uuid algorithm process

## 1.0.4

    * Add method Algorithm Process

## 1.0.2

    * Add HubkitClientInterface

## 1.0.1

    * Add possibility to change baseUrl

## 1.0.0

    * First stable version
