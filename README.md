# HubKit

HubKit is library written in PHP to communicate with hubkit.io.

- [HubKit](#hubkit)
    - [Features](#features)
    - [Requirements](#requirements)
    - [Component Libraries](#component-libraries)
    - [Installation](#installation)
        - [composer](#composer)
    - [Usage](#usage)
    - [Methods contains in HubkitSdk](#methods-contains-in-hubkitsdk)
    - [Credits](#credits)
    - [License](#license)

## Features

- [x] Easy setup and configuration
- [x] Comprehensive Unit Test Coverage

## Requirements

- Php version > 7.1

## Component Libraries

In order to keep HubKit focused specifically on its core functionnality implementations, additional component libraries are needed (but are contains in composer dependancies so you don't have to care about that).

- "guzzlehttp/psr7"
- "php-http/message"
- "php-http/multipart-stream-builder"

## Installation

### Composer

You can install it with the following command:

```bash
$ composer require hubkit/hk-sdk-php
```

## Usage

In order to use specific api functions, you just have to instanciate the `HubkitSdk` class and then call the willing method.

```php
use Hubkit\Sdk\HubkitSdk;

$hubkitSdk = new HubkitSdk();
$hubkitSdk->setApiKey('YOUR-HUBKIT-API-KEY');

$result = $hubkitSdk->me();

var_dump($result);
```

## Methods contains in HubkitSdk

### me();

### createDevice(array $datas);

    $datas = [
        'project' => 'UUID-HUBKIT-PROJECT',
        'name' => 'New Device from SDK',
        'macAddress' => 'FF:FF:FF:FF:FF:FF',
        'hardwareVersion' => 'v1',
        'firmwareVersion' => 'v2',
        'manualMode' => '1',
        'sensorType' => 'MY_TYPE_EXEMPLE',
        'battery' => '54',
        'factoryTest' => 'factory test',
        'latitude' => '1.5',
        'longitude' => '0.5',
        'externalIdentifier' => 'external_id',
    ];

### updateDevice(string $uuid, array $datas);

    $uuid = 'UUID-HUBKIT-DEVICE';

    // Here, the api is in PATCH method, all keys are optionnals
    $datas = [
        'name' => 'New Device from SDK',
        'macAddress' => 'FF:FF:FF:FF:FF:FF',
        'hardwareVersion' => 'v1',
        'firmwareVersion' => 'v2',
        'manualMode' => '1',
        'sensorType' => 'MY_TYPE_EXEMPLE',
        'battery' => '54',
        'factoryTest' => 'factory test',
        'latitude' => '1.5',
        'longitude' => '0.5',
        'externalIdentifier' => 'external_id',
    ];

### activateDevice(string $uuid);

    $uuid = 'UUID-HUBKIT-DEVICE';

### device(string $uuid);

    $uuid = 'UUID-HUBKIT-DEVICE';

### activity(string $uuid);

    $uuid = 'UUID-HUBKIT-ACTIVITY';

### allActivities();

### session(string $uuid);

    $uuid = 'UUID-HUBKIT-SESSION';

### deleteSession(string $uuid);

    $uuid = 'UUID-HUBKIT-SESSION';

### readySession(string $uuid);

    $uuid = 'UUID-HUBKIT-SESSION';

### createSession(string $uuid, array $datas);

    $uuid = 'UUID-HUBKIT-SESSION';

    $datas = [
        'project' => 'UUID-HUBKIT-PROJECT',
        'capturedAt' => '2018-01-01T01:00:00-05:00', // Care to format ISO 8601
        'metas' => [
            'meta1 name' => 'meta1 value',
            'meta2 name' => 'meta2 value',
            // ...
        ],
        'callbackUrl' => 'http://my.callback.com/route' // Callback launch after session decoded 
    ];

### rawData(string $uuid);

    $uuid = 'UUID-HUBKIT-RAWDATA';

### createRawData(array $datas, UploadedFileInterface $file);

    $datas = [
        'session' => 'UUID-HUBKIT-SESSION',
        'device' => 'UUID-HUBKIT-DEVICE',
    ];
    
    $file = '...' // must implement UploadedFileInterface

### algorithmProcess(string $uuid, array $datas);

    $datas = [
        'algorithm' => 'slug-algorithm',
        'dataType' => 'gps',
    ];

## Credits

HubKit is owned and maintained by the SURF'IN Company.

## License

HubKit is released under the MIT license. [See LICENSE](https://gitlab.com/hubkit/hk-sdk-php/blob/master/LICENSE) for details.

