<?php

namespace Hubkit\Sdk\Tests;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Client;
use Hubkit\Sdk\Response;
use Hubkit\Sdk\Query\QueryInterface;

/**
 * ClientTest
 *
 * @uses TestCase
 */
class ClientTest extends TestCase
{
    /**
     * queryMock
     *
     * @var Client
     */
    public $queryMock;

    /**
     * setUp
     */
    public function setUp(): void
    {
        $this->queryMock = $this->createMock(QueryInterface::class);

        /* $this->clientMock->method('send')->willReturn(json_encode($this->expectedReturnClient)); */
    }

    /**
     * testClientSendOk
     */
    public function testClientSendOk()
    {
        $client = new Client();

        $client->setMethod('GET');
        $client->setBody(json_encode([]));

        $res = $client->send('http://www.google.fr');

        $this->assertInstanceOf(Response::class, $res);
    }
}
