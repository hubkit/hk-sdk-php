<?php

namespace Hubkit\Sdk\Tests;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\HubkitSdk;
use Hubkit\Sdk\Client;
use Hubkit\Sdk\Response;
use Hubkit\Sdk\Tests\Mock\ResponseMock;
use GuzzleHttp\Psr7\UploadedFile;

/**
 * HubkitSdkTest
 *
 * @uses TestCase
 */
class HubkitSdkTest extends TestCase
{
    public $clientMock;

    public $responseMock;

    public $hubkitSdk;

    /**
     * setUp
     */
    public function setUp(): void
    {
        $this->clientMock = $this->createMock(Client::class);
        $this->responseMock = $this->createMock(Response::class);

        $this->hubkitSdk = new HubkitSdk($this->clientMock);
        $this->hubkitSdk->setApiKey('97bc072f-62ff-4c2f-a494-25df816e9e43');
    }

    /**
     * testMeOk
     */
    public function testMeOk()
    {
        $this->mockApiResponse('me.json');

        $res = $this->hubkitSdk->me();

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testCreateDevice
     */
    public function testCreateDevice()
    {
        $this->mockApiResponse('createDevice.json', 201);

        $res = $this->hubkitSdk->createDevice(
            [
                'project' => 'a48edeb7-a44d-437f-8f11-c83942d4d4eb',
                'name' => 'New Device from SDK',
                'macAddress' => '12:30:20',
                'hardwareVersion' => 'v1',
                'firmwareVersion' => 'v2',
                'manualMode' => '1',
                'sensorType' => 'MY_TYPE',
                'battery' => '54',
                'factoryTest' => 'factory test',
                'latitude' => '1.5',
                'longitude' => '0.5',
                'externalIdentifier' => 'external_id',
            ]
        );

        $this->assertEquals($res->getStatusCode(), 201);
    }

    /**
     * testUpdateDevice
     */
    public function testUpdateDevice()
    {
        $this->mockApiResponse('updateDevice.json');

        $res = $this->hubkitSdk->updateDevice(
            '95b5239b-b1ce-4856-b9b1-cad799a30b4c',
            [
                'name' => 'new name',
            ]
        );

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testActivateDevice
     */
    public function testActivateDevice()
    {
        $this->mockApiResponse('activateDevice.json');

        $res = $this->hubkitSdk->activateDevice('95b5239b-b1ce-4856-b9b1-cad799a30b4c');

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testDevice
     */
    public function testDevice()
    {
        $this->mockApiResponse('device.json');

        $res = $this->hubkitSdk->device('04887d2a-a532-496b-b482-4ecf47628859');

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testActivity
     */
    public function testActivity()
    {
        $this->mockApiResponse('activity.json');

        $res = $this->hubkitSdk->activity('28ed73d2-7b92-4ade-9b9e-bf8341ed3a3c');

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testActivities
     */
    public function testActivities()
    {
        $this->mockApiResponse('allActivities.json');

        $res = $this->hubkitSdk->allActivities();

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testSession
     */
    public function testSession()
    {
        $this->mockApiResponse('session.json');

        $res = $this->hubkitSdk->session('1724033a-573a-4e4e-957b-ce3a1917ae19');

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testReadySession
     */
    public function testReadySession()
    {
        $this->mockApiResponse('session.json');

        $res = $this->hubkitSdk->readySession('7954207b-2dfa-49dc-91cb-bbeb21072d64');

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testDeleteSession
     */
    public function testDeleteSession()
    {
        $this->mockApiResponse('session.json', 204);

        $res = $this->hubkitSdk->deleteSession('7954207b-2dfa-49dc-91cb-bbeb21072d64');

        $this->assertEquals($res->getStatusCode(), 204);
    }

    /**
     * testCreateSession
     */
    public function testCreateSession()
    {
        $this->mockApiResponse('createSession.json', 201);

        $res = $this->hubkitSdk->createSession([
            'project' => 'a48edeb7-a44d-437f-8f11-c83942d4d4eb',
            'capturedAt' => '2018-01-01T01:00:00-05:00',
            'metas' => [
                'meta1' => 'value1',
            ],
        ]);

        $this->assertEquals($res->getStatusCode(), 201);
    }

    /**
     * testRawData
     */
    public function testRawData()
    {
        $this->mockApiResponse('rawData.json');

        $res = $this->hubkitSdk->rawData('ce0124ee-686c-41e7-be85-1efbeaf2d9d3');

        $this->assertEquals($res->getStatusCode(), 200);
    }

    /**
     * testCreateRawData
     */
    public function testCreateRawData()
    {
        $this->mockApiResponse('createRawData.json', 201);

        $fileRawData = new UploadedFile(
            __DIR__.'/Mock/Files/rawdata.data',
            filesize(__DIR__.'/Mock/Files/rawdata.data'),
            0,
            'mySuperName.data'
        );

        $res = $this->hubkitSdk->createRawData(
            [
                'session' => '35e98030-df95-4642-a24a-2c8b6562ab5a',
                'device' => '04887d2a-a532-496b-b482-4ecf47628859',
            ],
            $fileRawData
        );

        $this->assertEquals($res->getStatusCode(), 201);
    }

    /**
     * testAlgorithmProcessData
     */
    public function testAlgorithmProcessData()
    {
        $this->mockApiResponse('algorithmProcess.json', 201);

        $res = $this->hubkitSdk->algorithmProcess(
            '35e98030-df95-4642-a24a-2c8b6562ab5a',
            [
                'algorithm' => 'my-algo',
                'dataType' => 'gps',
            ]
        );

        $this->assertEquals($res->getStatusCode(), 201);
    }

    /**
     * testMeWrongApiKey
     */
    public function testMeWrongApiKey()
    {
        $this->mockApiResponse('wrongApiKey.json', 401);

        $res = $this->hubkitSdk
            ->setApiKey('WrOnGAPIkey')
            ->me();

        $this->assertEquals($res->getStatusCode(), 401);
    }

    /**
     * mockApiResponse
     *
     * @param string $fileName
     * @param int    $responseCode
     */
    private function mockApiResponse(string $fileName, int $responseCode = 200)
    {
        $expected = file_get_contents(__DIR__.'/Mock/API/'.$fileName);

        $this->responseMock->method('getStatusCode')->willReturn($responseCode);
        $this->responseMock->method('getBody')->willReturn($expected);

        $this->clientMock->method('send')->willReturn($this->responseMock);
    }
}
