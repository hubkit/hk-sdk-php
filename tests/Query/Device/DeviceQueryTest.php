<?php

namespace Hubkit\Sdk\Tests\Query\Device;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Device\DeviceQuery;

/**
 * DeviceQueryTest
 *
 * @uses TestCase
 */
class DeviceQueryTest extends TestCase
{
    /**
     * testDeviceQuerySetters
     */
    public function testDeviceQuerySetters()
    {
        $deviceQuery = new DeviceQuery();
        $deviceQuery->setUuid('123456');

        $this->assertEquals('devices/123456', $deviceQuery->getUrl());
        $this->assertEquals('GET', $deviceQuery->getMethod());
    }
}
