<?php

namespace Hubkit\Sdk\Tests\Query\Device;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Device\ActivateDeviceQuery;

/**
 * ActivateDeviceQueryTest
 *
 * @uses TestCase
 */
class ActivateDeviceQueryTest extends TestCase
{
    /**
     * testActivateDeviceQuerySetters
     */
    public function testActivateDeviceQuerySetters()
    {
        $activateDeviceQuery = new ActivateDeviceQuery();
        $activateDeviceQuery->setUuid('123456');

        $this->assertEquals('devices/123456/activate', $activateDeviceQuery->getUrl());
        $this->assertEquals('PATCH', $activateDeviceQuery->getMethod());
    }
}
