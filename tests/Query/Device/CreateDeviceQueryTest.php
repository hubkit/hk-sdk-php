<?php

namespace Hubkit\Sdk\Tests\Query\Device;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Device\CreateDeviceQuery;

/**
 * CreateDeviceQueryTest
 *
 * @uses TestCase
 */
class CreateDeviceQueryTest extends TestCase
{
    /**
     * testCreateDeviceQuerySetters
     */
    public function testCreateDeviceQuerySetters()
    {
        $createDeviceQuery = new CreateDeviceQuery();
        $createDeviceQuery->setDatas([
            'attr1' => "value1",
        ]);

        json_decode($createDeviceQuery->getDatas());

        $this->assertEquals(true, json_last_error() === JSON_ERROR_NONE);
        $this->assertIsString($createDeviceQuery->getDatas());
        $this->assertEquals('devices', $createDeviceQuery->getUrl());
        $this->assertEquals('POST', $createDeviceQuery->getMethod());
    }
}
