<?php

namespace Hubkit\Sdk\Tests\Query\Device;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Device\UpdateDeviceQuery;

/**
 * UpdateDeviceQueryTest
 *
 * @uses TestCase
 */
class UpdateDeviceQueryTest extends TestCase
{
    /**
     * testUpdateDeviceQuerySetters
     */
    public function testUpdateDeviceQuerySetters()
    {
        $createDeviceQuery = new UpdateDeviceQuery();
        $createDeviceQuery->setUuid('123456');
        $createDeviceQuery->setDatas([
            'attr1' => "value1",
        ]);

        json_decode($createDeviceQuery->getDatas());

        $this->assertEquals(true, json_last_error() === JSON_ERROR_NONE);
        $this->assertIsString($createDeviceQuery->getDatas());
        $this->assertEquals('devices/123456', $createDeviceQuery->getUrl());
        $this->assertEquals('PATCH', $createDeviceQuery->getMethod());
    }
}
