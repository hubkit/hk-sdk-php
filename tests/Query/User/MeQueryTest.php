<?php

namespace Hubkit\Sdk\Tests\Query\User;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\User\MeQuery;

/**
 * MeQueryTest
 *
 * @uses TestCase
 */
class MeQueryTest extends TestCase
{
    /**
     * testMeQuerySetters
     */
    public function testMeQuerySetters()
    {
        $meQuery = new MeQuery();

        $this->assertEquals('me', $meQuery->getUrl());
        $this->assertEquals('GET', $meQuery->getMethod());
    }
}
