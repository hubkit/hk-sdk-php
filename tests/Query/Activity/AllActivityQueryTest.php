<?php

namespace Hubkit\Sdk\Tests\Query\Activity;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Activity\AllActivitiesQuery;

/**
 * AllActivityQueryTest
 *
 * @uses TestCase
 */
class AllActivityQueryTest extends TestCase
{
    /**
     * testAllActivityQuerySetters
     */
    public function testAllActivityQuerySetters()
    {
        $activityQuery = new AllActivitiesQuery();

        $this->assertEquals('activities', $activityQuery->getUrl());
        $this->assertEquals('GET', $activityQuery->getMethod());
    }
}
