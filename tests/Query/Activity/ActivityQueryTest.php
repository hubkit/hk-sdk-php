<?php

namespace Hubkit\Sdk\Tests\Query\Activity;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Activity\ActivityQuery;

/**
 * ActivityQueryTest
 *
 * @uses TestCase
 */
class ActivityQueryTest extends TestCase
{
    /**
     * testActivityQuerySetters
     */
    public function testActivityQuerySetters()
    {
        $activityQuery = new ActivityQuery();
        $activityQuery->setUuid('123456');

        $this->assertEquals('activities/123456', $activityQuery->getUrl());
        $this->assertEquals('GET', $activityQuery->getMethod());
    }
}
