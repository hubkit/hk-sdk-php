<?php

namespace Hubkit\Sdk\Tests\Query\RawData;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\RawData\RawDataQuery;

/**
 * RawDataQueryTest
 *
 * @uses TestCase
 */
class RawDataQueryTest extends TestCase
{
    /**
     * testRawDataQuerySetters
     */
    public function testRawDataQuerySetters()
    {
        $rawDataQuery = new RawDataQuery();
        $rawDataQuery->setUuid('123456');

        $this->assertEquals('raw_datas/123456', $rawDataQuery->getUrl());
        $this->assertEquals('GET', $rawDataQuery->getMethod());
    }
}
