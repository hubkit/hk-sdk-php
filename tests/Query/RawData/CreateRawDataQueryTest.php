<?php

namespace Hubkit\Sdk\Tests\Query\RawData;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\RawData\CreateRawDataQuery;

/**
 * CreateRawDataQueryTest
 *
 * @uses TestCase
 */
class CreateRawDataQueryTest extends TestCase
{
    /**
     * testCreateRawDataQuerySetters
     */
    public function testCreateRawDataQuerySetters()
    {
        $createRawDataQuery = new CreateRawDataQuery();
        $createRawDataQuery->setForm([
            'arrayForm',
        ]);
        $createRawDataQuery->setFile('arrayFile');

        $this->assertIsArray($createRawDataQuery->getForm());
        $this->assertIsArray($createRawDataQuery->getFiles());
        $this->assertEquals('raw_datas', $createRawDataQuery->getUrl());
        $this->assertEquals('POST', $createRawDataQuery->getMethod());
    }
}
