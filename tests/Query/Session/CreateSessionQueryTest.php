<?php

namespace Hubkit\Sdk\Tests\Query\Session;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Session\CreateSessionQuery;

/**
 * CreateSessionQueryTest
 *
 * @uses TestCase
 */
class CreateSessionQueryTest extends TestCase
{
    /**
     * testCreateSessionQuerySetters
     */
    public function testCreateSessionQuerySetters()
    {
        $createSessionQuery = new CreateSessionQuery();
        $createSessionQuery->setDatas([
            'arrayForm',
        ]);

        json_decode($createSessionQuery->getDatas());

        $this->assertEquals(true, json_last_error() === JSON_ERROR_NONE);
        $this->assertIsString($createSessionQuery->getDatas());
        $this->assertEquals('sessions', $createSessionQuery->getUrl());
        $this->assertEquals('POST', $createSessionQuery->getMethod());
    }
}
