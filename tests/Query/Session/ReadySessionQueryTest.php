<?php

namespace Hubkit\Sdk\Tests\Query\Session;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Session\ReadySessionQuery;

/**
 * ReadySessionQueryTest
 *
 * @uses TestCase
 */
class ReadySessionQueryTest extends TestCase
{
    /**
     * testReadySessionQuerySetters
     */
    public function testReadySessionQuerySetters()
    {
        $readySessionQuery = new ReadySessionQuery();
        $readySessionQuery->setUuid('123456');

        $this->assertEquals('sessions/123456/ready', $readySessionQuery->getUrl());
        $this->assertEquals('PATCH', $readySessionQuery->getMethod());
    }
}
