<?php

namespace Hubkit\Sdk\Tests\Query\Session;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Session\SessionQuery;

/**
 * SessionQueryTest
 *
 * @uses TestCase
 */
class SessionQueryTest extends TestCase
{
    /**
     * testSessionQuerySetters
     */
    public function testSessionQuerySetters()
    {
        $sessionQuery = new SessionQuery();
        $sessionQuery->setUuid('123456');

        $this->assertEquals('sessions/123456', $sessionQuery->getUrl());
        $this->assertEquals('GET', $sessionQuery->getMethod());
    }
}
