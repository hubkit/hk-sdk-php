<?php

namespace Hubkit\Sdk\Tests\Query\Session;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\Session\DeleteSessionQuery;

/**
 * DeleteSessionQueryTest
 *
 * @uses TestCase
 */
class DeleteSessionQueryTest extends TestCase
{
    /**
     * testReadySessionQuerySetters
     */
    public function testReadySessionQuerySetters()
    {
        $readySessionQuery = new DeleteSessionQuery();
        $readySessionQuery->setUuid('123456');

        $this->assertEquals('sessions/123456', $readySessionQuery->getUrl());
        $this->assertEquals('DELETE', $readySessionQuery->getMethod());
    }
}
