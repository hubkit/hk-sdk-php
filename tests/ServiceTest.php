<?php

namespace Hubkit\Sdk\Tests;

use PHPUnit\Framework\TestCase;
use Hubkit\Sdk\Query\QueryInterface;
use Hubkit\Sdk\Client;
use Hubkit\Sdk\Response;
use Hubkit\Sdk\Service;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

/**
 * ServiceTest
 *
 * @uses TestCase
 */
class ServiceTest extends TestCase
{
    /**
     * clientMock
     *
     * @var Client
     */
    public $clientMock;
    /**
     * queryMock
     *
     * @var Client
     */
    public $queryMock;

    /**
     * expectedReturnClient
     *
     * @var string
     */
    public $expectedReturnClient;

    /**
     * setUp
     */
    public function setUp(): void
    {
        $this->clientMock = $this->createMock(Client::class);
        $this->queryMock = $this->createMock(QueryInterface::class);
        $this->guzzleResponseMock = $this->createMock(GuzzleResponse::class);

        $this->clientMock->method('send')->willReturn(new Response($this->guzzleResponseMock));
    }

    /**
     * testServiceQuery
     */
    public function testServiceQuery()
    {
        $service = new Service($this->clientMock);

        $res = $service->query($this->queryMock);

        $this->assertInstanceOf(Response::class, $res);
    }

    /**
     * testServiceGenerateUrl
     */
    public function testServiceGenerateUrl()
    {
        $service = new Service($this->clientMock);

        $actionUrl = 'someresource/test';
        $finalUrl = $service->createUrl($actionUrl);
        $expected = $service->baseUrl.'/'.$actionUrl;

        $this->assertEquals($expected, $finalUrl);
    }

    /**
     * testServiceSetApiKeyOk
     *
     * @doesNotPerformAssertions
     */
    public function testServiceSetApiKeyOk()
    {
        $service = new Service($this->clientMock);

        $return = $service->setApiKey('myApiKey');
    }

    /**
     * testServiceSetApiKeyWrong
     */
    public function testServiceSetApiKeyWrong()
    {
        $this->expectException('TypeError');

        $service = new Service($this->clientMock);

        $return = $service->setApiKey([]);
    }
}
